const express = require("express");
const router = express.Router();
const auth = require("../auth");
const ProductController = require("../controllers/productControllers");

//Add a product (admin)
router.post("/add", auth.verify, (req, res) => {
    const data = {
        products: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin){
        ProductController.addProduct(req.body).then(result => res.send(result))
    } else {
        res.send({ auth: 'You are not authorize'})
    }
})


//Retrieved all active products
router.get("/active", (req, res) => {
    ProductController.getAllActive().then(result => res.send(result));
})


//Retrieved all products
router.get("/all", (req, res) => {
    ProductController.getAll().then(result => res.send(result));
})


//Retrieved specific order
router.get("/:id", (req, res) => {
    ProductController.getOne(req.params.id).then(result => res.send(result));
})


//Update a product
router.put("/update/:id", auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    } 
    if(data.isAdmin) {
        ProductController.updateProduct(req.params.id, req.body).then(result => res.send(result))
    } else {
        res.send('You are not authorize')
    }
})


//Archive product
router.put("/archive/:productId", auth.verify, (req, res) => {
    const data = {
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin){
        ProductController.archiveProduct(req.params.productId).then(result => res.send(result))
    } else {
        res.send('You are not authorize')
    }
})

//delete products
router.delete("/:id", auth.verify, (req, res) => {
    const userData = {
        isAdmin : auth.decode(req.headers.authorization).isAdmin,
    }
    if(userData.isAdmin){
       ProductController.deleteProduct(req.params.id).then(result => res.send(result));
    } else {
        res.send({auth : "You're not authorize to do this"})
    }
})

  


module.exports = router;