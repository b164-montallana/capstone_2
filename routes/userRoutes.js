const express = require("express");
const router = express.Router();
const UserController = require("../controllers/userControllers");
const User = require("../models/User");
const auth = require("../auth");

//register a user
router.post("/register", (req, res) => {
    UserController.registerUser(req.body).then(result => res.send(result));
})

//login a user
router.post("/login", (req, res) => {
    UserController.loginUser(req.body).then(result => res.send(result));
})

//get all users (admin only)
router.get("/all", auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    } 
    if(data.isAdmin){
        UserController.getAllUsers(data).then(result => res.send(result));
    }else{
        res.send({auth : 'You are not an admin'})
    }
})

//update a user change isAdmin: "from false to true" (admin only)
router.put("/admin_true/:id", auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    }
    if(data.isAdmin){
        UserController.setAdminTrue(req.params.id, req.body).then(result => res.send(result));
    } else {
        res.send({auth : "You're not authorize to do this"})
    }
})

router.put("/admin_false/:id", auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    }
    if(data.isAdmin){
        UserController.setAdminFalse(req.params.id, req.body).then(result => res.send(result));
    } else {
        res.send({auth : "You're not authorize to do this"})
    }
})

module.exports = router;