const express = require("express");
const router = express.Router();
const auth = require("../auth");
const OrderController = require("../controllers/orderController");

//create Order
router.post("/", auth.verify, (req, res) => {
    const userData = {
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }
    if(userData.isAdmin){
        res.send({auth : "You're not authorize to do this"})
    } else {
        OrderController.createOrder(userData, req.body).then(result => res.send(result))
    }
})

//
router.get("/all", auth.verify, (req, res) => {
    const userData = {
        isAdmin : auth.decode(req.headers.authorization).isAdmin,
    }
    if(userData.isAdmin){
        OrderController.getAll().then(result => res.send(result));
    } else {
        res.send({auth : "You're not authorize to do this"})
    }
})


//Retrieved user order
router.get("/myOrders", auth.verify, (req, res) => {
    const userData = {
        userId : auth.decode(req.headers.authorization).id,
    }
    OrderController.getOne(userData).then(result => res.send(result));
})

//delete order
router.delete("/:id", auth.verify, (req, res) => {
    const userData = {
        isAdmin : auth.decode(req.headers.authorization).isAdmin,
    }
    if(userData.isAdmin){
        OrderController.deleteOrder(req.params.id).then(result => res.send(result));
    } else {
        res.send({auth : "You're not authorize to do this"})
    }
})





module.exports = router;