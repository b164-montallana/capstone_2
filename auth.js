const jwt = require("jsonwebtoken");
require('dotenv').config();


module.exports.createAccessToken = (user) => {
	//The data will be received from the registration form
	//When the user logs in, a token will be created with user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, process.env.JWT_SECRET, {})
}

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	if(token){
		console.log(token);
		token = token.split(" ")[1];
		return jwt.verify(token, process.env.JWT_SECRET, (err, data) => {
			if(err) {
				return res.send({ auth: "failed" })
			}else {
				next()
			}
		})
	}else {
		return res.send({ auth: "token undefined" })
	}
}

module.exports.decode = (token) => {

	if(token){

		token = token.split(" ")[1];

		return jwt.verify(token, process.env.JWT_SECRET, (err, data) => {
			if(err) {
				return null
			} else {
				return jwt.decode(token, { complete:true }).payload;
			}
		})

	}else {
		return null
	}

}