const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, "name is required"]
        },
        description: {
            type: String,
            required: [true, "description is required"]
        },
        categories: {
            type: Array
        },
        price: {
            type: Number,
            required: [true, "Price is required"]
        },
        isActive: {
            type: Boolean,
            default: false
        }
    },
    {timestamps: true}
);

module.exports = mongoose.model("Product", productSchema)
