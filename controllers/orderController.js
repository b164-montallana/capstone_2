const Order = require("../models/Order");

//create order
module.exports.createOrder = (data, reqBody) => {
    let newOrder = new Order ({
        name: reqBody.name,
        userId : reqBody.userId,
        products : reqBody.products,
        amount : reqBody.amount,
        status : reqBody.status
    })
    return newOrder.save().then((order, error) => {
        if(error) {
            return error;
        } else {
            return order;
        }
    })
   
}

//retrieve all orders
module.exports.getAll = () => {
    return Order.find({}).select('userId products amount status' ).then(result => {
        return result;
    })
}

//Retrieve user product
module.exports.getOne = (data) => {
    return Order.find({ userId : data.userId}).then(result => {
        return result;
    })
}

//delete an order

module.exports.deleteOrder = (reqParams) => {
    return Order.findByIdAndDelete(reqParams).then(result => {
        return 'Order deleted';
    })
}