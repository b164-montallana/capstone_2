const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//user registration
module.exports.registerUser = (reqBody) => {
    return User.findOne({ email: reqBody.email}).then(result => {
        if(result){
            return `Sorry, ${result.email} already exist`
        } else {
            let newUser = new User({
                firstName: reqBody.firstName,
                lastName: reqBody.lastName,
                email: reqBody.email,
                password: bcrypt.hashSync(reqBody.password, 12),
                isAdmin: reqBody.isAdmin
            })
            return newUser.save().then((user, error) => {
                if(error){
                    return false;
                } else {
                    return `Welcome! registration successful`;
                }
            })
        }
    })  
}   

//Login a user
module.exports.loginUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).select('-firstName -lastName').then(result => {
		if(result == null){
			return 'Invalid email';
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            const {password, ...others} = result._doc
			if(isPasswordCorrect){
				return {
                    ...others, accessToken : auth.createAccessToken(result.toObject())
                    }
			} else {
				return 'Invalid password';
			}
		}
	})
}

//get all users (admin only)
module.exports.getAllUsers = (data) => {
    return User.find({}).select('-password').then(result => {
        return result;
    })
}

//Set a user as admin (admin only)
module.exports.setAdminTrue = (reqParams) => {
    let updateAsAdmin = {
        isAdmin: true
    };
    return User.findByIdAndUpdate(reqParams, updateAsAdmin).select('-password').then((user, error) => {
        if(error){
            return 'Invalid user';
        } else {
            return 'isAdmin : true';
        }
    })
}

//disable Admin
module.exports.setAdminFalse = (reqParams) => {
    let updateAsAdmin = {
        isAdmin: false
    };
    return User.findByIdAndUpdate(reqParams, updateAsAdmin).select('-password').then((user, error) => {
        if(error){
            return 'Invalid user';
        } else {
            return 'isAdmin : false';
        }
    })
}
