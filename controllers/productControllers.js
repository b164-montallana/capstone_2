const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");

//add a product (admin)
module.exports.addProduct = (reqBody) => {
    let newProduct = new Product ({
        name: reqBody.name,
        description: reqBody.description,
        categories: reqBody.categories,
        price: reqBody.price,
        isActive: reqBody.isActive
    })
    return newProduct.save().then((product, error) => {
        if(error){
            return false;
        } else {
            return `${product.name} has been added`;
        }
    })
}

//retrieve all active products
module.exports.getAllActive = () => {
    return Product.find({ isActive: true }).select('-categories').then(result => {
        return result;
    })
}

//retrieve all products
module.exports.getAll = () => {
    return Product.find({}).select('-categories').then(result => {
        return result;
    })
}

//Retrieve specipic product
module.exports.getOne = (reqParams) => {
    return Product.findById(reqParams).then(result => {
        return result;
    })
}

//Update a Product
module.exports.updateProduct = (productId, reqBody) => {
    let updatedProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        isActive: reqBody.isActive
    };
    return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
        if(error){
            return 'failed to update';
        } else {
            return 'Product has been updated';
        }
    })
}

//Archive a product
module.exports.archiveProduct = (reqParams) => {
    let updateActiveField = {
        isActive : false
    }
    return Product.findByIdAndUpdate(reqParams, updateActiveField).then((product, error)=> {
        if(error){
            return false;
        } else {
            return true;
        }
    })
}


module.exports.deleteProduct = (reqParams) => {
    return Product.findByIdAndDelete(reqParams).then(result => {
        return 'Product has been deleted';
    })
}