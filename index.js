const express = require("express");
const mongoose = require("mongoose");
require('dotenv').config();
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/product.Routes");
const orderRoutes = require("./routes/orderRoutes");

const app = express();


app.use(cors());
app.use(express.json());
app.use(express.urlencoded( {  extended: true } ));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/order", orderRoutes);

mongoose.connect(process.env.DB_CONNECTION, {
    useNewUrlParser: true,
    UseUnifiedTopology: true
})


mongoose.connection.once('open', () => console.log('You are now connected to mongoDB Atlas'));

app.listen(process.env.PORT, () => {
    console.log(`E-CommerceAPI is now online on port ${process.env.PORT}`);
})